# My personal Arch Linux tips

## Archinstall

Create your base configuration using `archinstall --dry-run`

Save `user_configuration.json` to a drive, you can acess while installation process.

I use Ventoy USB flash drive, which has second partition for data. Also you can use an existing partition on your HDD or SSD, or another USB flash drive. 

I renamed my config file to `arch_user_config.json`.

```bash
mkdir -p mnt
mount /dev/sda3 mnt
archinstall --config ./mnt/arch_user_config.json
```

## Chroot

1. Create a mount directory `mkdir -p mymnt`

2. Mount your drive volume to the directory `mount /dev/vda1 mymnt`

3. Go into the mount directory `cd mymnt`

4. Run the command: `chroot . sh`

## Silent boot

#### systemd-boot configuration file:

```
/boot/loader/entries/arch.conf
```

add kernel parameters to the `options` line:

#### Kernel parameters by Arch Wiki:

```
quiet loglevel=3 systemd.show_status=auto rd.udev.log_level=3 
```

#### Kernel parameters by me:

```
quiet loglevel=3 systemd.show_status=false
```
